/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd.h"
#include "bme.h"
#include "nrf24.h"
#include "ds3231.h"
#include "debug.h"
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define TIMEOUT_1_SEC ((uint16_t) 1000U)
#define HOURS_TO_MINUTES	(60U)
#define TRUE 	1U
#define FALSE 	0U

#define BITS_IN_BYTE	(8U)
#define HOUR_IN_DAY		(24U)
#define MINUTES_IN_HOUR	(60U)
#define MINUTES_IN_DAY 	(HOUR_IN_DAY * MINUTES_IN_HOUR)


#define WORK_LOW_POWER

#define NRF_DATA_LENGTH	(32U)
#define NRF_CHANNEL	(2U)

typedef enum
{
	SIGNAL_STATUS_OFF = 0,
	SIGNAL_STATUS_ON = 1,
}signal_status_T;

typedef enum
{
	INIT = 0,
	IDLE = 1,
	BUSSY = 2
}state_T;

typedef enum
{
	LIGHT_DETECT = 0,
	LIGHT_ON = 1,
	LIGHT_OFF = 2
}light_T;

typedef enum
{
	SIGNAL_DEBUG_COMMAND = 0,
	SIGNAL_INT_DETECT,
	SIGNAL_RF_RX,
	SIGNAL_INT_TIME,
	N_SIGNALS
}signal_types_T;

typedef union
{
	console_state_T console_state;
}function_T;

typedef struct
{
	signal_status_T status;
	function_T function;
	uint8_t	data[NRF_DATA_LENGTH];
	uint16_t size;
}signal_T;

typedef enum
{
	REQ_RTC_ALL = 0,
	REQ_RTC_DAY_MONTH,
	REQ_RTC_DAY_WEEK,
	REQ_RTC_MONTH,
	REQ_RTC_YEAR,
	REQ_RTC_HOUR,
	REQ_RTC_MINUTE,
	REQ_RTC_SECOND
}data_req_rtc_T;

typedef enum
{
	REQ_BME_ALL = 0,
	REQ_BME_TEMPERATURE,
	REQ_BME_HUMIDITY,
	REQ_BME_PRESSURE
}data_req_bme_T;

typedef enum
{
	REQ_DATE_CURRENT = 0,
	REQ_DATE_INITIAL
}req_date_T;

static signal_T signals[N_SIGNALS];
static state_T state = INIT;
static light_T light = LIGHT_DETECT;
static ds3231_T initial_time;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_I2C1_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
#ifdef WORK_LOW_POWER
static void EntryLowPower(void);
#endif
static void StateTransition(void);
static void StateDo(void);
static void CheckDetectPin(void);
static void Init(void);
static void DetectInt(uint8_t detect);
static void DebugCommandCallback(console_state_T command, uint8_t* data, uint16_t size);
static void DebugDateRequested(data_req_rtc_T request);
static void DebugTemperatureRequested(data_req_bme_T request);
static void SendDetectStatus(void);
static void NrfTemperatureRequested(void);
static void NrfDateRequested(req_date_T date_requested);
static void DebugExecuteCommand(void);
static void NrfSendData(uint8_t * data, uint8_t size);
static void NrfDateUpdate(uint8_t* rx);
static void NrfReceiveCommand(uint8_t* rx);
static void LightCommand(uint8_t data);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#ifdef WORK_LOW_POWER
static void EntryLowPower(void)
{
    HAL_SuspendTick();

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
	//HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

    /* Resume Tick interrupt if disabled prior to sleep mode entry*/
    HAL_ResumeTick();
}
#endif

static void DebugDateRequested(data_req_rtc_T request)
{
	ds3231_T time;

	Ds3231GetTime(&time);

	switch(request)
	{
	case REQ_RTC_ALL:
		DebugSendString("RTC -> ");
		DebugSendString(time.char_day);
		DebugSendString(". ");
		DebugSendString(time.day_week_str);
		DebugSendString(". ");
		DebugSendString(time.char_hour);
		DebugSendString("\n\r");
		break;
	case REQ_RTC_DAY_MONTH:
		DebugSendUNumber(time.day_month);
		DebugSendString("\r\n");
		break;
	case REQ_RTC_DAY_WEEK:
		DebugSendString(time.day_week_str);
		DebugSendString("\r\n");
		break;
	case REQ_RTC_HOUR:
		DebugSendUNumber(time.hour);
		DebugSendString("\r\n");
		break;
	case REQ_RTC_MINUTE:
		DebugSendUNumber(time.minutes);
		DebugSendString("\r\n");
		break;
	case REQ_RTC_MONTH:
		DebugSendUNumber(time.month);
		DebugSendString("\r\n");
		break;
	case REQ_RTC_SECOND:
		DebugSendUNumber(time.seconds);
		DebugSendString("\r\n");
		break;
	case REQ_RTC_YEAR:
		DebugSendUNumber(time.year);
		DebugSendString("\r\n");
		break;
	default:
		break;
	}
}

static void DebugTemperatureRequested(data_req_bme_T request)
{
	bme_data_T bme_data;

	if(BmeGetData(&bme_data) == BME_OK)
	{

		switch(request)
		{
			case REQ_BME_ALL:
				DebugSendString("BME -> T:");
				DebugSendString(bme_data.temp_string);
				DebugSendString(" H:");
				DebugSendString(bme_data.hum_string);
				DebugSendString(" P:");
				DebugSendString(bme_data.press_string);
				DebugSendString("\r\n");
				break;
			case REQ_BME_HUMIDITY:
				DebugSendString(bme_data.hum_string);
				DebugSendString("\r\n");
				break;
			case REQ_BME_PRESSURE:
				DebugSendString(bme_data.press_string);
				DebugSendString("\r\n");
				break;
			case REQ_BME_TEMPERATURE:
				DebugSendString(bme_data.temp_string);
				DebugSendString("\r\n");
				break;
			default:
				break;
		}
	}
	else
	{
		DebugSendString("\r\nBME READ ERROR!!!\r\nT:");
	}
}

static void SendDetectStatus(void)
{
	uint8_t detect = 0;

	detect = HAL_GPIO_ReadPin(DETECT_GPIO_Port, DETECT_Pin) +'0';
	NrfSendData(&detect, sizeof(detect));
}

static void NrfTemperatureRequested(void)
{
	bme_data_T bme_data;
	uint8_t tx_data[NRF_DATA_LENGTH];
	uint8_t total_size = 0;
	uint8_t size = 0;

	DebugSendString("T requested\r\n");
	if(BmeGetData(&bme_data) == BME_OK)
	{
		size = sizeof("T:") - 1;
		memcpy(tx_data, "T:", size);
		total_size = size;

		size = sizeof(bme_data.temp_string) - 1;
		memcpy(&tx_data[total_size], bme_data.temp_string, size);
		total_size += size;

		size = sizeof(" H:") - 1;
		memcpy(&tx_data[total_size], " H:", size);
		total_size += size;

		size = sizeof(bme_data.hum_string) - 1;
		memcpy(&tx_data[total_size], bme_data.hum_string, size);
		total_size += size;

		size = sizeof(" P:") - 1;
		memcpy(&tx_data[total_size], " P:", size);
		total_size += size;

		size = sizeof(bme_data.press_string) - 1;
		memcpy(&tx_data[total_size], bme_data.press_string, size);
		total_size += size;
	}
	else
	{
		total_size = sizeof("BME READ ERROR!!!\r\n");
		memcpy(tx_data, "BME READ ERROR!!!\r\n", total_size);

		DebugSendString("\r\nBME READ ERROR!!!\r\nT:");
	}

	NrfSendData(tx_data, total_size);
}

static void NrfDateRequested(req_date_T date_requested)
{
	uint8_t tx_data[NRF_DATA_LENGTH];
	uint8_t total_size = 0;
	uint8_t size = 0;
	ds3231_T time;
	ds3231_T* time_to_send = NULL;

	if(date_requested == REQ_DATE_CURRENT)
	{
		time_to_send = &time;
		Ds3231GetTime(&time);
	}
	else
	{
		time_to_send = &initial_time;
	}

	DebugSendString("Date requested\r\n");
	size = sizeof(time_to_send->char_day) - 1;
	memcpy(&tx_data[total_size], time_to_send->char_day, size);
	total_size += size;

	tx_data[total_size] = '.';
	total_size ++;

	size = sizeof(time_to_send->day_week_str) - 1;
	memcpy(&tx_data[total_size], time_to_send->day_week_str, size);
	total_size += size;

	tx_data[total_size] = '.';
	total_size ++;

	size = sizeof(time_to_send->char_hour) - 1;
	memcpy(&tx_data[total_size], time_to_send->char_hour, size);
	total_size += size;

	NrfSendData(tx_data, total_size);
}

static void NrfSendData(uint8_t * data, uint8_t size)
{
	uint8_t output[NRF_DATA_LENGTH];
	uint8_t i = 0;
	uint8_t result = NRF24_MESSAGE_LOST;
	uint8_t crc = 0;
	uint8_t max_tries = 20;

	memset(output, 0, NRF_DATA_LENGTH);

	DebugSendString("Sending");
	if(size < (NRF_DATA_LENGTH - 2))
	{
		output[0] = size;
		crc = 0;
		for(i = 0; i < size; i ++)
		{
			output[i + 1] = data[i];
			crc += data[i];
		}
		if (crc != 0)
		{
			crc = (uint8_t)(256 - (uint16_t)crc);
		}
		output[size + 1] = crc;

		i = 0;
		while((result == NRF24_MESSAGE_LOST) && (i < max_tries))
		{
			nrf24_send(output);
			while(nrf24_isSending());
			result = nrf24_lastMessageStatus();
			i ++;
		}
		if(i == max_tries)
		{
			DebugSendString("Error sending NRF");
		}
	}

	nrf24_powerUpRx();
}

static void NrfDateUpdate(uint8_t* rx)
{
	ds3231_T time;
	uint8_t ack_command[2] = {'O', 'K'};
	/* 01 23 4 56 7 89 A B C DE F 1011 12 1314 */
	/* SD dd / mm / yy . w . hh :  mm  :   ss */
	if((rx[0x04] == '/') && (rx[0x07] == '/') && (rx[0x0A] == '.') && (rx[0x0C] == '.') &&
			(rx[0x0F] == ':') && (rx[0x12] == ':'))
	{
		time.day_month = ((rx[0x02] - '0') * 10) + (rx[0x03] - '0');
		time.month = ((rx[0x05] - '0') * 10) + (rx[0x06] - '0');
		time.year = ((rx[0x08] - '0') * 10) + (rx[0x09] - '0');
		time.day_week = (rx[0x0B] - '0');
		time.hour = ((rx[0x0D] - '0') * 10) + (rx[0x0E] - '0');
		time.minutes = ((rx[0x10] - '0') * 10) + (rx[0x11] - '0');
		time.seconds = ((rx[0x13] - '0') * 10) + (rx[0x14] - '0');

		if((time.day_month >= 1) && (time.day_month <= 31) && (time.month >= 1) && (time.month <= 12) &&
				(time.year < 100) && (time.day_week >= 1) && (time.day_week <= 7) && (time.hour < 24) &&
				(time.minutes < 60) && (time.seconds < 60))
		{
			Ds3231SetTime(time);
		}
		NrfSendData(ack_command, sizeof(ack_command));
	}
}

static void DebugExecuteCommand(void)
{
	switch(signals[SIGNAL_DEBUG_COMMAND].function.console_state)
	{
	case CONSOLE_SET_DATE_DAY_MONTH_DATA:
		Ds3231SetSingleReg(DS3231_DAY_MONTH, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_SET_DATE_DAY_WEEK_DATA:
		Ds3231SetSingleReg(DS3231_DAY_WEEK, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_SET_DATE_MONTH_DATA:
		Ds3231SetSingleReg(DS3231_MONTH, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_SET_DATE_YEAR_DATA:
		Ds3231SetSingleReg(DS3231_YEAR, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_SET_DATE_HOUR_DATA:
		Ds3231SetSingleReg(DS3231_HOUR, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_SET_DATE_MINUTE_DATA:
		Ds3231SetSingleReg(DS3231_MINUTE, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_SET_DATE_SECOND_DATA:
		Ds3231SetSingleReg(DS3231_SECOND, signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	case CONSOLE_GET_DATE_COMPLETE:
		DebugDateRequested(REQ_RTC_ALL);
		break;
	case CONSOLE_GET_DATE_DAY_MONTH:
		DebugDateRequested(REQ_RTC_DAY_MONTH);
		break;
	case CONSOLE_GET_DATE_DAY_WEEK:
		DebugDateRequested(REQ_RTC_DAY_WEEK);
		break;
	case CONSOLE_GET_DATE_MONTH:
		DebugDateRequested(REQ_RTC_MONTH);
		break;
	case CONSOLE_GET_DATE_YEAR:
		DebugDateRequested(REQ_RTC_YEAR);
		break;
	case CONSOLE_GET_DATE_HOUR:
		DebugDateRequested(REQ_RTC_HOUR);
		break;
	case CONSOLE_GET_DATE_MINUTE:
		DebugDateRequested(REQ_RTC_MINUTE);
		break;
	case CONSOLE_GET_DATE_SECOND:
		DebugDateRequested(REQ_RTC_SECOND);
		break;
	case CONSOLE_GET_BME_COMPLETE:
		DebugTemperatureRequested(REQ_BME_ALL);
		break;
	case CONSOLE_GET_BME_TEMPERATURE:
		DebugTemperatureRequested(REQ_BME_TEMPERATURE);
		break;
	case CONSOLE_GET_BME_HUMIDITY:
		DebugTemperatureRequested(REQ_BME_HUMIDITY);
		break;
	case CONSOLE_GET_BME_PRESSURE:
		DebugTemperatureRequested(REQ_BME_PRESSURE);
			break;
	case CONSOLE_STANDBY:
	case CONSOLE_INIT:
	case CONSOLE_SET:
	case CONSOLE_GET:
	case CONSOLE_SET_DATE:
	case CONSOLE_SET_DATE_DAY_MONTH:
	case CONSOLE_SET_DATE_DAY_WEEK:
	case CONSOLE_SET_DATE_MONTH:
	case CONSOLE_SET_DATE_YEAR:
	case CONSOLE_SET_DATE_HOUR:
	case CONSOLE_SET_DATE_MINUTE:
	case CONSOLE_SET_DATE_SECOND:
	case CONSOLE_GET_DATE:
	case CONSOLE_GET_BME:
	case CONSOLE_SET_NRF24_LENGTH:
	case CONSOLE_SET_NRF24_DATA:
	case CONSOLE_SET_LIGHT:
		DebugConsolePrint(signals[SIGNAL_DEBUG_COMMAND].function.console_state);
		break;
	case CONSOLE_SET_NRF24_SEND_DATA:
		NrfSendData(signals[SIGNAL_DEBUG_COMMAND].data, signals[SIGNAL_DEBUG_COMMAND].size);
		break;
	case CONSOLE_SET_LIGHT_COMMAND:
		LightCommand(signals[SIGNAL_DEBUG_COMMAND].data[0]);
		break;
	default:
		break;
	}
}

static void NrfReceiveCommand(uint8_t* rx)
{
	uint8_t i = 0;
	uint8_t crc = 0;

	for(i = 0; i < (rx[0] + 1); i ++)
	{
		crc += rx[i + 1];
	}

	if(crc == 0)
	{
		switch(rx[1])
		{
			case 'G':
				switch(rx[2])
				{
					case 'D':
						NrfDateRequested(REQ_DATE_CURRENT);
						break;
					case 'T':
						NrfTemperatureRequested();
						break;
					case 'P':
						SendDetectStatus();
						break;
					case 'I':
						NrfDateRequested(REQ_DATE_INITIAL);
						break;
					default:
						break;
				}
				break;
			case 'S':
				switch(rx[2])
				{
					case 'R':
						HAL_NVIC_SystemReset();
						break;
					case 'D':
						NrfDateUpdate(&rx[1]);
						break;
					case 'L':
						LightCommand(rx[3]);
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
	else
	{
		DebugSendString("Incorrect Checksum\r\n");
	}
}

static void LightCommand(uint8_t data)
{
	uint8_t ack_command[2] = {'O', 'K'};
	switch(data)
	{
		case '0':
			light = LIGHT_OFF;
			HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_SET);
			break;
		case '1':
			light = LIGHT_ON;
			HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_RESET);
			break;
		case 'D':
			light = LIGHT_DETECT;
			CheckDetectPin();
			break;
		default:
			break;
	}
	NrfSendData(ack_command, sizeof(ack_command));
}

static void CheckDetectPin(void)
{
	signals[SIGNAL_INT_DETECT].data[0] = HAL_GPIO_ReadPin(DETECT_GPIO_Port, DETECT_Pin);
	signals[SIGNAL_INT_DETECT].status = SIGNAL_STATUS_ON;
	signals[SIGNAL_INT_DETECT].size = 1;
}

static void Init(void)
{
	uint8_t i = 0;
	uint8_t tx_address[5] = {0xE7,0xE7,0xE7,0xE7,0xE7};
	uint8_t rx_address[5] = {0xD7,0xD7,0xD7,0xD7,0xD7};
	uint8_t error = 0;

	for(i = 0; i < N_SIGNALS; i ++)
	{
		signals[i].status = SIGNAL_STATUS_OFF;
		signals[i].size = 0;
	}
	DebugInit(&huart2, &DebugCommandCallback);
	DebugSendString("STARTUP\n\r");

	HAL_TIM_Base_Start_IT(&htim3);
	if(BmeInit(&hi2c1) == BME_OK)
	{
		DebugSendString("BME STARTED\n\r");
	}
	else
	{
		error ++;
		DebugSendString("BME STARTING ERROR\n\r");
	}
	Ds3231Init(&hi2c1);
	DebugSendString("RTC STARTED\n\r");

	nrf24_init(&hspi1, NRF24_CE_GPIO_Port, NRF24_CE_Pin, NRF24_CSN_GPIO_Port, NRF24_CSN_Pin);
	nrf24_config(NRF_CHANNEL, NRF_DATA_LENGTH);
	nrf24_tx_address(tx_address);
	nrf24_rx_address(rx_address);

	nrf24_flush_rx();
	nrf24_flush_tx();
	nrf24_powerUpRx();

	if(nrf24_getStatus() == 14)
	{
		DebugSendString("NRF STARTED\n\r");
	}
	else
	{
		error ++;
		DebugSendString("NRF STARTING ERROR\n\r");
	}

	if(error != 0)
	{
		HAL_NVIC_SystemReset();
	}

	DebugSendString("Press + to open debug console\r\n");

	Ds3231GetTime(&initial_time);
	CheckDetectPin();
}

static void DebugCommandCallback(console_state_T command, uint8_t* data, uint16_t size)
{
	uint16_t i = 0;
	if(signals[SIGNAL_DEBUG_COMMAND].status == SIGNAL_STATUS_OFF)
	{
		signals[SIGNAL_DEBUG_COMMAND].status = SIGNAL_STATUS_ON;
		signals[SIGNAL_DEBUG_COMMAND].function.console_state = command;
		signals[SIGNAL_DEBUG_COMMAND].size = size;
		for(i = 0; i < size; i ++)
		{
			signals[SIGNAL_DEBUG_COMMAND].data[i] = data[i];
		}
	}
}

static void DetectInt(uint8_t detect)
{
	ds3231_T time;
	uint16_t low_time, high_time, current_time;
	uint8_t sun_time [12][31][4] = {{{9,4,18,13},{9,5,18,14},{9,5,18,15},{9,5,18,16},{9,5,18,17},{9,4,18,18},{9,4,18,19},{9,4,18,20},{9,4,18,21},{9,4,18,22},{9,4,18,23},{9,3,18,24},{9,3,18,25},{9,2,18,26},{9,2,18,27},{9,2,18,29},{9,1,18,30},{9,1,18,31},{9,0,18,32},{8,59,18,33},{8,59,18,35},{8,58,18,36},{8,57,18,37},{8,57,18,38},{8,56,18,40},{8,55,18,41},{8,54,18,42},{8,53,18,44},{8,52,18,45},{8,52,18,46},{8,51,18,47}},{{8,50,18,49},{8,49,18,50},{8,47,18,51},{8,46,18,53},{8,45,18,54},{8,44,18,55},{8,43,18,57},{8,42,18,58},{8,41,18,59},{8,39,19,1},{8,38,19,2},{8,37,19,3},{8,36,19,5},{8,34,19,6},{8,33,19,7},{8,31,19,8},{8,30,19,10},{8,29,19,11},{8,27,19,12},{8,26,19,13},{8,24,19,15},{8,23,19,16},{8,21,19,17},{8,20,19,19},{8,18,19,20},{8,17,19,21},{8,15,19,22},{8,14,19,23},{8,14,19,23},{8,14,19,23},{8,14,19,23}},{{7,12,18,25},{7,10,18,26},{7,9,18,27},{7,7,18,28},{7,5,18,29},{7,4,18,31},{7,2,18,32},{7,0,18,33},{6,59,18,34},{6,57,18,35},{6,55,18,36},{6,54,18,38},{6,52,18,39},{6,50,18,40},{6,49,18,41},{6,47,18,42},{6,45,18,43},{6,43,18,44},{6,42,18,46},{6,40,18,47},{6,38,18,48},{6,36,18,49},{6,35,18,50},{6,33,18,51},{6,31,18,52},{6,29,18,53},{6,28,18,55},{6,26,18,56},{6,24,18,57},{6,22,18,58},{6,21,18,59}},{{6,19,19,0},{6,17,19,1},{6,15,19,2},{6,14,19,3},{6,12,19,5},{6,10,19,6},{6,9,19,7},{6,7,19,8},{6,5,19,9},{6,4,19,10},{6,2,19,11},{6,0,19,12},{5,59,19,14},{5,57,19,15},{5,55,19,16},{5,54,19,17},{5,52,19,18},{5,51,19,19},{5,49,19,20},{5,48,19,21},{5,46,19,23},{5,45,19,24},{5,43,19,25},{5,42,19,26},{5,40,19,27},{5,39,19,28},{5,37,19,29},{5,36,19,30},{5,34,19,32},{5,33,19,33},{5,33,19,33}},{{5,32,19,34},{5,30,19,35},{5,29,19,36},{5,28,19,37},{5,26,19,38},{5,25,19,39},{5,24,19,40},{5,23,19,42},{5,22,19,43},{5,20,19,44},{5,19,19,45},{5,18,19,46},{5,17,19,47},{5,16,19,48},{5,15,19,49},{5,14,19,50},{5,13,19,51},{5,12,19,52},{5,11,19,53},{5,10,19,54},{5,10,19,55},{5,9,19,56},{5,8,19,57},{5,7,19,58},{5,6,19,59},{5,6,20,0},{5,5,20,1},{5,4,20,2},{5,4,20,3},{5,3,20,3},{5,3,20,4}},{{5,2,20,5},{5,2,20,6},{5,1,20,7},{5,1,20,7},{5,1,20,8},{5,0,20,9},{5,0,20,9},{5,0,20,10},{4,59,20,11},{4,59,20,11},{4,59,20,12},{4,59,20,12},{4,59,20,13},{4,59,20,13},{4,59,20,14},{4,59,20,14},{4,59,20,14},{4,59,20,15},{4,59,20,15},{4,59,20,15},{4,59,20,15},{4,59,20,16},{5,0,20,16},{5,0,20,16},{5,0,20,16},{5,1,20,16},{5,1,20,16},{5,1,20,16},{5,2,20,16},{5,2,20,16},{5,2,20,16}},{{5,3,20,16},{5,3,20,16},{5,4,20,16},{5,4,20,15},{5,5,20,15},{5,5,20,15},{5,6,20,15},{5,7,20,14},{5,7,20,14},{5,8,20,13},{5,9,20,13},{5,10,20,12},{5,10,20,12},{5,11,20,11},{5,12,20,11},{5,13,20,10},{5,14,20,9},{5,15,20,9},{5,15,20,8},{5,16,20,7},{5,17,20,7},{5,18,20,6},{5,19,20,5},{5,20,20,4},{5,21,20,3},{5,22,20,2},{5,23,20,1},{5,24,20,0},{5,25,19,59},{5,26,19,58},{5,27,19,57}},{{5,28,19,56},{5,29,19,55},{5,30,19,54},{5,31,19,53},{5,32,19,51},{5,33,19,50},{5,34,19,49},{5,35,19,48},{5,36,19,46},{5,37,19,45},{5,38,19,44},{5,39,19,42},{5,41,19,41},{5,42,19,40},{5,43,19,38},{5,44,19,37},{5,45,19,35},{5,46,19,34},{5,47,19,32},{5,48,19,31},{5,49,19,29},{5,50,19,28},{5,51,19,26},{5,52,19,25},{5,53,19,23},{5,54,19,21},{5,55,19,20},{5,56,19,18},{5,58,19,16},{5,59,19,15},{6,0,19,13}},{{6,1,19,11},{6,2,19,10},{6,3,19,8},{6,4,19,6},{6,5,19,5},{6,6,19,3},{6,7,19,1},{6,8,18,59},{6,9,18,58},{6,10,18,56},{6,11,18,54},{6,12,18,52},{6,13,18,51},{6,14,18,49},{6,15,18,47},{6,16,18,45},{6,17,18,44},{6,18,18,42},{6,19,18,40},{6,20,18,38},{6,21,18,36},{6,23,18,35},{6,24,18,33},{6,25,18,31},{6,26,18,29},{6,27,18,28},{7,28,19,26},{7,29,19,24},{7,30,19,22},{7,31,19,21},{7,31,19,21}},{{7,32,19,19},{7,33,19,17},{7,34,19,15},{7,35,19,14},{7,36,19,12},{7,38,19,10},{7,39,19,8},{7,40,19,7},{7,41,19,5},{7,42,19,3},{7,43,19,2},{7,44,19,0},{7,45,18,58},{7,46,18,57},{7,48,18,55},{7,49,18,54},{7,50,18,52},{7,51,18,50},{7,52,18,49},{7,53,18,47},{7,55,18,46},{7,56,18,44},{7,57,18,43},{7,58,18,41},{7,59,18,40},{8,1,18,38},{8,2,18,37},{8,3,18,36},{8,4,18,34},{8,6,18,33},{8,7,18,32}},{{8,8,18,30},{8,9,18,29},{8,11,18,28},{8,12,18,27},{8,13,18,25},{8,14,18,24},{8,16,18,23},{8,17,18,22},{8,18,18,21},{8,19,18,20},{8,21,18,19},{8,22,18,18},{8,23,18,17},{8,24,18,16},{8,26,18,15},{8,27,18,14},{8,28,18,13},{8,29,18,12},{8,31,18,12},{8,32,18,11},{8,33,18,10},{8,34,18,9},{8,36,18,9},{8,37,18,8},{8,38,18,8},{8,39,18,7},{8,40,18,7},{8,41,18,6},{8,42,18,6},{8,44,18,5},{8,44,18,5}},{{8,45,18,5},{8,46,18,5},{8,47,18,4},{8,48,18,4},{8,49,18,4},{8,50,18,4},{8,51,18,4},{8,52,18,4},{8,52,18,4},{8,53,18,4},{8,54,18,4},{8,55,18,4},{8,56,18,4},{8,57,18,4},{8,57,18,4},{8,58,18,4},{8,59,18,5},{8,59,18,5},{9,0,18,5},{9,0,18,6},{9,1,18,6},{9,2,18,7},{9,2,18,7},{9,2,18,8},{9,3,18,8},{9,3,18,9},{9,3,18,9},{9,4,18,10},{9,4,18,11},{9,4,18,12},{9,4,18,12}}};
	uint8_t nrf_tx[NRF_DATA_LENGTH] = "";
	uint8_t size = 0;
	uint8_t total_size = 0;

	if(detect == 1)
	{
		Ds3231GetTime(&time);
		low_time = (sun_time[time.month - 1][time.day_month - 1][0] * HOURS_TO_MINUTES) + sun_time[time.month - 1][time.day_month - 1][1];
		high_time = (sun_time[time.month - 1][time.day_month - 1][2] * HOURS_TO_MINUTES) + sun_time[time.month - 1][time.day_month - 1][3];
		current_time = time.hour * HOURS_TO_MINUTES + time.minutes;

		if((current_time < low_time) || (current_time > high_time))
		{
			DebugSendString("NIGHT!");

			if(light == LIGHT_DETECT)
			{
				HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_RESET);
			}
		}
		else
		{
			DebugSendString("DAY!");
		}

		total_size = sizeof("DETECT -> ") - 1;
		memcpy(&nrf_tx[0], "DETECT -> ", total_size);

		size = sizeof(time.char_hour) - 1;
		memcpy(&nrf_tx[total_size], time.char_hour, size);
		total_size += size;

		NrfSendData(nrf_tx, total_size);
	}
	else
	{
		DebugSendString("End Detect");
		if(light == LIGHT_DETECT)
		{
			HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_SET);
		}
	}
}

static void StateTransition(void)
{
    if(nrf24_dataReady() == 1)
    {
    	signals[SIGNAL_RF_RX].status = SIGNAL_STATUS_ON;
    	signals[SIGNAL_RF_RX].size = NRF_DATA_LENGTH;
    	nrf24_getData(signals[SIGNAL_RF_RX].data);
    }

	switch(state)
	{
		case IDLE:
			if((signals[SIGNAL_INT_DETECT].status == SIGNAL_STATUS_ON) ||
					(signals[SIGNAL_DEBUG_COMMAND].status == SIGNAL_STATUS_ON) ||
					(signals[SIGNAL_RF_RX].status == SIGNAL_STATUS_ON) ||
					(signals[SIGNAL_INT_TIME].status == SIGNAL_STATUS_ON))
			{
				state = BUSSY;
			}
			break;
		case BUSSY:
			if((signals[SIGNAL_INT_DETECT].status == SIGNAL_STATUS_OFF) &&
					(signals[SIGNAL_DEBUG_COMMAND].status == SIGNAL_STATUS_OFF) &&
					(signals[SIGNAL_RF_RX].status == SIGNAL_STATUS_OFF) &&
					(signals[SIGNAL_INT_TIME].status == SIGNAL_STATUS_OFF))
			{
				state = IDLE;
			}
			break;
		default:
			break;
	}
}

static void StateDo(void)
{
	switch(state)
	{
		case IDLE:
#ifdef WORK_LOW_POWER
			EntryLowPower();
#endif
			break;
		case BUSSY:
			if(signals[SIGNAL_RF_RX].status == SIGNAL_STATUS_ON)
			{
				NrfReceiveCommand(signals[SIGNAL_RF_RX].data);
				signals[SIGNAL_RF_RX].status = SIGNAL_STATUS_OFF;
			}
			else if(signals[SIGNAL_INT_DETECT].status == SIGNAL_STATUS_ON)
			{
				DetectInt(signals[SIGNAL_INT_DETECT].data[0]);
				signals[SIGNAL_INT_DETECT].status = SIGNAL_STATUS_OFF;
			}
			else if(signals[SIGNAL_DEBUG_COMMAND].status == SIGNAL_STATUS_ON)
			{
				DebugExecuteCommand();
				signals[SIGNAL_DEBUG_COMMAND].status = SIGNAL_STATUS_OFF;
			}
			else if(signals[SIGNAL_INT_TIME].status == SIGNAL_STATUS_ON)
			{
				HAL_GPIO_TogglePin(LED_BOARD_GPIO_Port, LED_BOARD_Pin);

				signals[SIGNAL_INT_TIME].status = SIGNAL_STATUS_OFF;
			}
			else
			{
				/* Do Nothing */
			}
			break;
		default:
			break;
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	signals[SIGNAL_INT_DETECT].data[0] = HAL_GPIO_ReadPin(DETECT_GPIO_Port, DETECT_Pin);
	signals[SIGNAL_INT_DETECT].status = SIGNAL_STATUS_ON;
	signals[SIGNAL_INT_DETECT].size = 1;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{

	signals[SIGNAL_INT_TIME].status = SIGNAL_STATUS_ON;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_TIM3_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  HAL_GPIO_WritePin(LED_BOARD_GPIO_Port, LED_BOARD_Pin, GPIO_PIN_RESET);
  HAL_Delay(2000);
  Init();
  HAL_GPIO_WritePin(LED_BOARD_GPIO_Port, LED_BOARD_Pin, GPIO_PIN_SET);
  state = IDLE;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  StateTransition();
	  StateDo();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 61;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65040;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_BOARD_GPIO_Port, LED_BOARD_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(NRF24_CSN_GPIO_Port, NRF24_CSN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(NRF24_CE_GPIO_Port, NRF24_CE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : LED_BOARD_Pin RELAY_Pin */
  GPIO_InitStruct.Pin = LED_BOARD_Pin|RELAY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : DETECT_Pin */
  GPIO_InitStruct.Pin = DETECT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(DETECT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : NRF24_CSN_Pin */
  GPIO_InitStruct.Pin = NRF24_CSN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(NRF24_CSN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : NRF24_CE_Pin */
  GPIO_InitStruct.Pin = NRF24_CE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(NRF24_CE_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
