/*
 * debug.c
 *
 *  Created on: 11 dec, 2018
 *      Author: roi
 */

#include "stm32f1xx_hal.h"
#include "stdbool.h"
#include "debug.h"
#include "stdlib.h"

#define COMMAND_POS			(1U)
#define SUB_FUNCTION_POS	(2U)

#define	MAX_DATA			(200U)
#define N_BUFFER			(2U)
#define MAX_COMMAND_LENGTH	(32U)

#define ASCII_CLEAN_SCREEN	(12U)
#define TX_TIMEOUT			(500U)

typedef enum
{
	BUFFER_READY = 0,
	BUFFER_TRANSMITTING
}buffer_status_T;

typedef struct
{
	buffer_status_T status;
	uint8_t	size;
	uint8_t data[MAX_DATA];
}dma_buffer_T;

typedef struct
{
	dma_buffer_T buff[N_BUFFER];
	dma_buffer_T* now;
	dma_buffer_T* next;
}out_T;

static out_T out;
static UART_HandleTypeDef * uart1_p;
static uint8_t debug_on = true;
static uint8_t rx_data = 0;
static CommandCallback_T CommandCallback = NULL;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	static uint8_t size = 0;
	static uint8_t rx_buffer[MAX_COMMAND_LENGTH];
	static console_state_T console_state = CONSOLE_STANDBY;
	console_state_T previous_state = console_state;
	uint8_t data_value = 0;
	static uint8_t tx_size = 0;

	if(rx_data == '*')
	{
		console_state = CONSOLE_STANDBY;
	}
	else
	{
		switch(console_state)
		{
			case CONSOLE_STANDBY:
				if(rx_data == '+')
				{
					console_state = CONSOLE_INIT;
				}
				break;
			case CONSOLE_INIT:
				switch(rx_data)
				{
					case '1':
						console_state = CONSOLE_SET;
						break;
					case '2':
						console_state = CONSOLE_GET;
						break;
					default:
						break;
				}
				break;
			case CONSOLE_SET:
				switch(rx_data)
				{
					case '0':
						console_state = CONSOLE_INIT;
						break;
					case '1':
						HAL_NVIC_SystemReset();
						break;
					case '2':
						console_state = CONSOLE_SET_DATE;
						break;
					case '3':
						console_state = CONSOLE_SET_NRF24_LENGTH;
						break;
					case '4':
						console_state = CONSOLE_SET_LIGHT;
						break;
					default:
						break;
				}
				break;
			case CONSOLE_GET:
				switch(rx_data)
				{
					case '0':
						console_state = CONSOLE_INIT;
						break;
					case '1':
						console_state = CONSOLE_GET_DATE;
						break;
					case '2':
						console_state = CONSOLE_GET_BME;
						break;
					default:
						break;
				}
				break;
			case CONSOLE_SET_DATE:
				switch(rx_data)
				{
					case '0':
						console_state = CONSOLE_SET;
						break;
					case '1':
						console_state = CONSOLE_SET_DATE_DAY_MONTH;
						break;
					case '2':
						console_state = CONSOLE_SET_DATE_DAY_WEEK;
						break;
					case '3':
						console_state = CONSOLE_SET_DATE_MONTH;
						break;
					case '4':
						console_state = CONSOLE_SET_DATE_YEAR;
						break;
					case '5':
						console_state = CONSOLE_SET_DATE_HOUR;
						break;
					case '6':
						console_state = CONSOLE_SET_DATE_MINUTE;
						break;
					case '7':
						console_state = CONSOLE_SET_DATE_SECOND;
						break;
					default:
						break;
				}
				break;
			case CONSOLE_SET_DATE_DAY_MONTH:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						data_value = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_SET_DATE_DAY_MONTH_DATA, &data_value, sizeof(data_value));
						size = 0;
					}
				}
				break;
			case CONSOLE_SET_DATE_DAY_WEEK:
				if((rx_data >= '1') && (rx_data <= '7'))
				{
					data_value = rx_data - '0';
					console_state = CONSOLE_STANDBY;
					previous_state = CONSOLE_STANDBY;
					CommandCallback(CONSOLE_SET_DATE_DAY_WEEK_DATA, &data_value, sizeof(data_value));
					size = 0;
				}
				break;
			case CONSOLE_SET_DATE_MONTH:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						data_value = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_SET_DATE_MONTH_DATA, &data_value, sizeof(data_value));
						size = 0;
					}
				}
				break;
			case CONSOLE_SET_DATE_YEAR:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						data_value = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_SET_DATE_YEAR_DATA, &data_value, sizeof(data_value));
						size = 0;
					}
				}
				break;
			case CONSOLE_SET_DATE_HOUR:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						data_value = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_SET_DATE_HOUR_DATA, &data_value, sizeof(data_value));
						size = 0;
					}
				}
				break;
			case CONSOLE_SET_DATE_MINUTE:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						data_value = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_SET_DATE_MINUTE_DATA, &data_value, sizeof(data_value));
						size = 0;
					}
				}
				break;
			case CONSOLE_SET_DATE_SECOND:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						data_value = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_SET_DATE_SECOND_DATA, &data_value, sizeof(data_value));
						size = 0;
					}
				}
				break;
			case CONSOLE_GET_DATE:
				switch(rx_data)
				{
					case '0':
						console_state = CONSOLE_GET;
						break;
					case '1':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_COMPLETE, rx_buffer, size);
						break;
					case '2':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_DAY_MONTH, rx_buffer, size);
						break;
					case '3':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_DAY_WEEK, rx_buffer, size);
						break;
					case '4':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_MONTH, rx_buffer, size);
						break;
					case '5':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_YEAR, rx_buffer, size);
						break;
					case '6':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_HOUR, rx_buffer, size);
						break;
					case '7':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_MINUTE, rx_buffer, size);
						break;
					case '8':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_DATE_SECOND, rx_buffer, size);
						break;
					default:
						break;
				}
				break;
			case CONSOLE_GET_BME:
				switch(rx_data)
				{
					case '0':
						console_state = CONSOLE_GET;
						break;
					case '1':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_BME_COMPLETE, rx_buffer, size);
						break;
					case '2':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_BME_TEMPERATURE, rx_buffer, size);
						break;
					case '3':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_BME_HUMIDITY, rx_buffer, size);
						break;
					case '4':
						console_state = CONSOLE_STANDBY;
						previous_state = CONSOLE_STANDBY;
						CommandCallback(CONSOLE_GET_BME_PRESSURE, rx_buffer, size);
						break;
					default:
						break;
				}
				break;
			case CONSOLE_SET_NRF24_LENGTH:
				if((rx_data >= '0') && (rx_data <= '9'))
				{
					rx_buffer[size] = rx_data - '0';
					size ++;
					if(size == 2)
					{
						tx_size = (rx_buffer[0] * 10) + rx_buffer[1];
						console_state = CONSOLE_SET_NRF24_DATA;
						size = 0;
					}
				}
				break;
			case CONSOLE_SET_NRF24_DATA:
				rx_buffer[size] = rx_data;
				size ++;
				if(size == tx_size)
				{
					console_state = CONSOLE_STANDBY;
					previous_state = CONSOLE_STANDBY;
					CommandCallback(CONSOLE_SET_NRF24_SEND_DATA, rx_buffer, tx_size);
					size = 0;
				}
				break;
			case CONSOLE_SET_LIGHT:
				if(rx_data != '0')
				{
					if(rx_data == '2')
					{
						rx_data = '0';
					}
					else if (rx_data == '3')
					{
						rx_data = 'D';
					}
					else
					{
						/* Do Nothing */
					}
					console_state = CONSOLE_STANDBY;
					previous_state = CONSOLE_STANDBY;
					CommandCallback(CONSOLE_SET_LIGHT_COMMAND, &rx_data, sizeof(rx_data));
				}
				else
				{
					console_state = CONSOLE_SET;
				}
				break;
			default:
				break;
		}
	}

	if(previous_state != console_state)
	{
		CommandCallback(console_state, rx_buffer, size);
	}

	HAL_UART_Receive_IT(uart1_p, &rx_data, sizeof(rx_data));
}


void DebugConsolePrint(console_state_T state)
{
	char consolePrint[CONSOLE_N_SCREENS][MAX_DATA] = {
			"\r\nPress + to open debug console\r\n",
			"\r\nDebug console:\r\n1) Set\r\n2) Get\r\nPress * to close debug console in any moment\r\n",
			"\r\nSet:\r\n0) Back\r\n1) Reset\r\n2) Date\r\n3) NRF24\r\n4) LIGHT\r\n",
			"\r\nGet:\r\n0) Back\r\n1) Get RTC data\r\n2) Get BME data\r\n",
			"\r\nSet RTC data:\r\n0) Back\r\n1) Set RTC day month\r\n2) Set RTC day week\r\n3) Set RTC month\r\n4) Set RTC year\r\n5) Set RTC hour\r\n6) Set RTC minute\r\n7) Set RTC second\r\n",
			"\r\nWrite day month (01-31): DD\r\n",
			"",
			"\r\nWrite day week (1-7): D\r\n",
			"",
			"\r\nWrite month (01-12): MM\r\n",
			"",
			"\r\nWrite year (00-99): YY\r\n",
			"",
			"\r\nWrite hour (00-23): HH\r\n",
			"",
			"\r\nWrite minute (00-59): MM\r\n",
			"",
			"\r\nWrite second (00-59): SS\r\n",
			"",
			"\r\nGet RTC data:\r\n0) Back\r\n1) Get RTC complete\r\n2) Get RTC day month\r\n3) Get RTC day week\r\n4) Get RTC month\r\n5) Get RTC year\r\n6) Get RTC hour\r\n7) Get RTC minute\r\n8) Get RTC second\r\n",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\r\nGet BME data:\r\n0) Back\r\n1) Get BME complete\r\n2) Get BME temperature\r\n3) Get BME humidity\r\n4) Get BME pressure\r\n",
			"",
			"",
			"",
			"",
			"\r\nWrite size to send (00-32):\r\n",
			"\r\nWrite data to send:\r\n",
			"",
			"\r\nSet Light:\r\n0) Back\r\n1) ON\r\n2) OFF\r\n3) DETECTION\r\n",
			""
	};

	DebugSendString(consolePrint[state]);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	dma_buffer_T* aux_buff_p;

	out.next->status = BUFFER_READY;
	if(out.now->size != 0)
	{
		out.now->status = BUFFER_TRANSMITTING;
		HAL_UART_Transmit_DMA(uart1_p, out.now->data, out.now->size);
		out.now->size = 0;
		aux_buff_p = out.now;
		out.now = out.next;
		out.next = aux_buff_p;
	}
}

void DebugInit(UART_HandleTypeDef* uart1, CommandCallback_T callback)
{
	uart1_p = uart1;
	uint8_t clean_screen = ASCII_CLEAN_SCREEN;
	CommandCallback = callback;

	out.now = &out.buff[0];
	out.next = &out.buff[1];
	out.buff[0].size = 0;
	out.buff[0].status = BUFFER_READY;
	out.buff[1].size = 0;
	out.buff[1].status = BUFFER_READY;

	HAL_UART_Transmit(uart1_p, &clean_screen, sizeof(clean_screen), TX_TIMEOUT);
	HAL_UART_Receive_IT(uart1_p, &rx_data, sizeof(rx_data));
}

void DebugSendString(const char * text)
{
	uint16_t index = 0;
	dma_buffer_T* aux_buff_p;

	if(debug_on == true)
	{
		HAL_NVIC_DisableIRQ(DMA1_Channel7_IRQn);
		while(text[index] != 0)
		{
			if(out.now->size < MAX_DATA)
			{
				out.now->data[out.now->size] = text[index];
				out.now->size ++;
				index ++;
			}
			else if((out.next->status == BUFFER_READY) && out.next->size < MAX_DATA)
			{
				out.next->data[out.next->size] = text[index];
				out.next->size ++;
				index ++;
			}
			else
			{
				while(1)
				{
					if(HAL_UART_Transmit(uart1_p, (uint8_t*) "ERROR -> INCREASE_DEBUG_BUFFER\r\n", sizeof("ERROR -> INCREASE_DEBUG_BUFFER\r\n"), 500) == HAL_OK)
					{
						while(1);
					}
				}
			}
		}
		if((out.next->status == BUFFER_READY) && (out.now->size != 0))
		{
			out.now->status = BUFFER_TRANSMITTING;
			HAL_UART_Transmit_DMA(uart1_p, out.now->data, out.now->size);
			out.now->size = 0;

			aux_buff_p = out.now;
			out.now = out.next;
			out.next = aux_buff_p;
		}
		HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
	}
}

void DebugSendUNumber(uint32_t number)
{
	char num_string[11];

	itoa(number, num_string, 10);
	DebugSendString(num_string);
}
